package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import business.idadeEnderecoRN;
import model.idadeEndereco;

/**
 * Servlet implementation class CadastroServlet
 */

public class CadastroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private idadeEnderecoRN business = new idadeEnderecoRN();
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CadastroServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int idade = Integer.parseInt(request.getParameter("idade"));
		String cep = request.getParameter("cep");
		idadeEndereco novoObjeto = new idadeEndereco(idade, cep);
		business.criarIdadeEndereco(novoObjeto);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO FAZER O CÓDIGO DA VALIDAÇÃO AQUI
	    String idade = request.getParameter("idade");
	    String cep = request.getParameter("cep");
		String mensagem = "";

		if(idade.equals("")){
			mensagem = "Idade não pode em branco\n";	
		}
		if(cep.equals("")){
			mensagem += "cep não pode em branco";
		}
		if(!mensagem.equals("")){
			PrintWriter out = response.getWriter();
			response.setContentType("text/xml");
			out.println("<response>");
			out.println("<mensagem>"+ mensagem+"</mensagem>");
			out.println("</response>");
			out.close();
			response.sendRedirect("/ValidaForm/cadastroForm.jsp");
		}
		else {
			doGet(request, response);
		}

	}

}
