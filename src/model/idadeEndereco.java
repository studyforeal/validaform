package model;

public class idadeEndereco {

	private int idade;
	private String cep;
	public idadeEndereco() {

	}
	public idadeEndereco(int idade, String cep) {
		this.idade = idade;
		this.cep = cep;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
}
