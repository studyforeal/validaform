<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script>
	var xmlHttp = new XMLHttpRequest();
	
	function valida(){
		
		var idadeStr = document.forms[0].idade.value
		var cepStr = document.forms[0].cep.value;
		
		var url = "/ValidaForm/validaCadastro?idade=" + idadeStr + "&cep=" +cepStr;
		
		
		
		xmlHttp.open("POST", url, true);
		xmlHttp.onreadystatechange = function(){
			
			if((xmlHttp.readyState == 4) && (xmlHttp.status == 200)){
				
				if(xmlHttp.responseXML != null){
					alert(xmlHttp.responseXML.getElementsByTagName("mensagem")[0].firstChild.data);
				}
			}
		} 
		
		xmlHttp.send();
	}
	
	function somar(idade){
		
		
		var idadeDigitada = idade.value;
		
		var url = "/ValidaForm/somaServlet?idade=" + idadeDigitada ;
		
		xmlHttp.open("GET", url, true);
		xmlHttp.onreadystatechange = function(){
			
			if((xmlHttp.readyState == 4) && (xmlHttp.status == 200)){
				
				if(xmlHttp.responseXML != null){
					document.getElementById("retornoIdade").innerHTML = xmlHttp.responseXML.getElementsByTagName("idadeSomada")[0].firstChild.data;
				}

			}
		} 
		
		xmlHttp.send();
		
		
	}

</script>

</head>
<body>

<form name="form1" method="get" action="/ValidaForm/validaCadastro">
	<table>
		<tr>
			<td>Idade: </td> <td><input type="text" name="idade" onBlur="somar(this)"></td>
			<div id="retornoIdade">Aqui ser� a soma da idade</div>
		</tr>
		<tr>
			<td>CEP:</td> <td> <input type="text" name="cep"></td>
		</tr>
		<tr>
			<td>
			<input type="button" value="Cadastrar" onClick="valida()">&nbsp;&nbsp;
				<input type="reset" value="Limpar"> </td>
		</tr>	
	
	</table>
</form>

</body>
</html>